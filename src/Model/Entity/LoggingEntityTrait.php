<?php
namespace Riddlemd\Logging\Model\Entity;

// Requires Riddlemd/Tools/Model/Entity/CachingEntityTrait
trait LoggingEntityTrait
{
    public function getLogsOfType(string $type, string $subtype = null, bool $resetCache = false)
    {
        $type = strtoupper($type . ($subtype ? "/{$subtype}" : null));
        $key = __FUNCTION__ . ':' . $type;
        if(!$this->isInCache($key) || $resetCache)
        {
            $logs = [];
            if(isset($this->logs) && is_array($this->logs))
            {
                $logs = array_filter($this->logs, function($v) use ($type) {
                    return $v->type == $type;
                });
            }
            $this->writeToCache($key, $logs);
        }

        return $this->readFromCache($key);
    }

    public function getNewestLogOfType(string $type, string $subtype = null, bool $resetCache = false)
    {
        $type = strtoupper($type . ($subtype ? "/{$subtype}" : null));
        $key = __FUNCTION__ . ':' . $type;
        if(!$this->isInCache($key) || $resetCache)
        {
            $logs = $this->getLogsOfType($type);
            $this->writeToCache($key, current($logs) ?: null);
        }

        return $this->readFromCache($key);
    }

    public function getOldestLogOfType(string $type, string $subtype = null, bool $resetCache = false)
    {
        $type = strtoupper($type . ($subtype ? "/{$subtype}" : null));
        $key = __FUNCTION__ . ':' . $type;
        if(!$this->isInCache($key) || $resetCache)
        {
            $logs = $this->getLogsOfType($type);
            $this->writeToCache($key, end($logs) ?: null);
        }

        return $this->readFromCache($key);
    }

    public function getLogTypeCount(string $type, string $subtype = null, bool $resetCache = false)
    {
        $type = strtoupper($type . ($subtype ? "/{$subtype}" : null));
        $key = __FUNCTION__ . ':' . $type;
        if(!$this->isInCache($key) || $resetCache)
        {
            $logs = $this->getLogsOfType($type);
            $this->writeToCache($key, count($logs));
        }

        return $this->readFromCache($key);
    }
}