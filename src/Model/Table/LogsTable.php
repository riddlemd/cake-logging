<?php
namespace Riddlemd\Logging\Model\Table;

use Cake\ORM\Table as BaseTable;
use Cake\Utility\Inflector;

class LogsTable extends BaseTable
{
    public function initialize(array $config)
    {
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        parent::initialize($config);

        $this->addBehavior('Timestamp');
    }

    public function write(string $type, ?array $data = null, array $options = [])
    {
        $options = array_merge([
            'fkResource' => null,
            'fkId' => null,
            'userId' => null
        ], $options);

        $type = explode('.', strtoupper($type));

        $log = $this->newEntity();
        $log->type = $type[0];
        $log->sub_type = $type[1] ?? null;

        foreach($options as $name => $value)
        {
            $name = Inflector::underscore($name);
            $log->$name = $value;
        }

        $log->data = $data;


        return $this->save($log) ? $log : false;
    }
}