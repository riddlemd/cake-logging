<?php
namespace Riddlemd\Logging\Model\Behavior;

use Cake\ORM\Behavior as BaseBehavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Table;

class LoggingBehavior extends BaseBehavior
{
    protected $_defaultConfig = [
        'logTableClassName' => 'Riddlemd/Logging.Logs',
        'propertiesToIgnore' => [
            'modified'
        ]
    ];

    public function __construct(Table $table, array $config = [])
    {
        parent::__construct($table, $config);
    }

    public function initialize(array $config)
    {
        parent::initialize($config);

        if(!isset($this->_table->Logs))
        {
            $this->_table->hasMany('Logs', [
                'className' => $this->getConfig('logTableClassName'),
                'foreignKey' => 'fk_id',
                'conditions' => [
                    'fk_resource' => $this->getTable()->getTable()
                ],
                'sort' => [
                    'Logs.created' => 'DESC'
                ]
            ]);
        }
    }

    public function write(string $type, ?array $data = null, array $options = [])
    {
        $options = array_merge([
            'fkResource' => $this->getTable()->getTable()
        ], $options);
        return $this->_table->Logs->write($type, $data, $options);
    }

    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if(!$entity->isNew() || isset($options['logging']) && !$options['logging'])
            return;

        $primaryKey = $this->_table->getPrimaryKey();

        $this->write('CREATE', null, [
            'fkId' => $entity->$primaryKey,
            'userId' => isset($options['user']) ? $options['user']->id : null
        ]);
    }

    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if($entity->isNew() || isset($options['logging']) && !$options['logging'])
            return;

        $schema = $this->_table->getSchema();
        $data = [];

        foreach($entity->getDirty() as $dirtyPropertyName)
        {
            if(in_array($dirtyPropertyName, $this->getConfig('propertiesToIgnore')))
                continue;
            
            $newValue = $entity->$dirtyPropertyName;
            $oldValue = $entity->getOriginal($dirtyPropertyName);

            if($newValue === $oldValue)
                continue;
            
            $data[$dirtyPropertyName] = $oldValue;
        }

        if(empty($data))
            return;

        $primaryKey = $this->_table->getPrimaryKey();

        $this->write('MODIFY', $data, [
            'fkId' => $entity->$primaryKey,
            'userId' => isset($options['user']) ? $options['user']->id : null
        ]);
    }

    public function beforeDelete(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $primaryKey = $this->_table->getPrimaryKey();

        $this->write('DELETE', [], [
            'fkId' => $entity->$primaryKey,
            'userId' => isset($options['user']) ? $options['user']->id : null
        ]);
    }
}