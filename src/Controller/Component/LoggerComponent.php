<?php
namespace Riddlemd\Logging\Controller\Component;

use Cake\Controller\Component as BaseComponent;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class LoggerComponent extends BaseComponent
{
    protected $_defaultConfig = [
        'tableClassName' => 'Riddlemd/Logging.Logs',
        'propertiesToIgnore' => [
            'modified'
        ]
    ];

    protected $Logs;

    public function initialize(array $config = [])
    {
        parent::initialize($config);

        $controller = $this->getController();
        $this->Logs = $controller->loadModel($this->getConfig('tableClassName'));
    }

    public function write(string $type, ?array $data = null, array $options = [])
    {
        if(empty($options['fkResource']))
        {
            $backtrace = debug_backtrace();
            foreach($backtrace as $trace)
            {
                if(is_a($trace['object'], '\Cake\Controller\Controller'))
                {
                    $options['fkResourceType'] = 'CONTROLLER';
                    $options['fkResource'] = get_class($trace['object']);
                }
            }
        }

        return $this->Logs->write($type, $data, $options);
    }
}